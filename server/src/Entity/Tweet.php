<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TweetRepository")
 */
class Tweet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $twitter_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $screen_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date_created;

    /**
     * @ORM\Column(type="string", length=280)
     */
    private $text;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTwitterId(): ?int
    {
        return $this->twitter_id;
    }

    public function setTwitterId(int $twitter_id): self
    {
        $this->twitter_id = $twitter_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getScreenName(): ?string
    {
        return $this->screen_name;
    }

    public function setScreenName(string $screen_name): self
    {
        $this->screen_name = $screen_name;

        return $this;
    }

    public function getDateCreated(): ?string
    {
        return $this->date_created;
    }

    public function setDateCreated(string $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }
}
