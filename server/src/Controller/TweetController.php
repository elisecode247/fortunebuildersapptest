<?php
namespace App\Controller;

use App\Entity\Tweet;
use App\Repository\TweetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use DG\Twitter\Twitter;

class TweetController extends ApiController
{
    /**
    * @Route("/tweets/hashtags/{q}", methods="GET")
    */
    public function search($q,Request $request)
    {
        if (! $request) {
            return $this->respondValidationError('Please provide a valid request!');
        }
        $consumerKey = getenv('TWITTER_C_K');
        $consumerSecret = getenv('TWITTER_C_S');
        $accessToken = getenv('TWITTER_A_T');
        $accessTokenSecret = getenv('TWITTER_T_S');
        
        $twitter = new Twitter($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
        $tweets =  $twitter->request('search/tweets', 'GET', ['q' => $q,'count' => 20]);
        $tweetsArray = [];
        foreach ($tweets->statuses as $tweet) {
            $tweetsArray[] = $this->transformTwitterResults($tweet);
        }
        return $this->respond($tweetsArray);
    }

    /**
    * @Route("/tweets/saved", methods="GET")
    */
    public function index(TweetRepository $tweetRepository)
    {
        $tweets = $tweetRepository->transformAll();

        return $this->respond($tweets);
    }

    /**
    * @Route("/tweets/post", methods="POST")
    */
    public function create(Request $request, TweetRepository $tweetRepository, EntityManagerInterface $em)
    {
        $request = $this->transformJsonBody($request);
        //error_log(print_r($request,1));
        if (! $request) {
            return $this->respondValidationError('Please provide a valid request!');
        }

        // persist the new tweet
        $tweet = new Tweet;

        $tweet->setTwitterId($request->request->get('twitter_id'));
        $tweet->setName($request->request->get('name'));
        $tweet->setScreenName($request->request->get('screen_name'));
        $tweet->setDateCreated($request->request->get('date_created'));
        $tweet->setText($request->request->get('text'));

        $em->persist($tweet);
        $em->flush();

        return $this->respondCreated('success');
    }

    /**
    * @Route("/tweets/delete/{twitter_id}", methods="DELETE")
    */
    public function delete($twitter_id,Request $request, TweetRepository $tweetRepository, EntityManagerInterface $em)
    {
        $tweet = $tweetRepository->findOneBySomeField($twitter_id);
        if (!$tweet) {
            return $this->respondNotFound();
        }

        $em->remove($tweet);
        $em->flush();

        return $this->respondCreated('deleted');
    }

    private function transformTwitterResults($tweet){
        return [
            'twitter_id' => (string) $tweet->id_str,
            'name' => (string) $tweet->user->name,
            'screen_name' => (string) $tweet->user->screen_name,
            'date_created' => (string) $tweet->created_at,
            'text' => (string) $tweet->text,
        ];
    }

}