<?php

namespace App\Repository;

use App\Entity\Tweet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Tweet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tweet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tweet[]    findAll()
 * @method Tweet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TweetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tweet::class);
    }

    public function transform(Tweet $tweet)
    {
        return [
            'twitter_id' => (string) $tweet->getTwitterId(),
            'name' => (string) $tweet->getName(),
            'screen_name' => (string) $tweet->getScreenName(),
            'date_created' => (string) $tweet->getDateCreated(),
            'text' => (string) $tweet->getText(),
        ];
    }

    public function transformAll()
    {
        $tweets = $this->findAll();
        $tweetsArray = [];

        foreach ($tweets as $tweet) {
            $tweetsArray[] = $this->transform($tweet);
        }

        return $tweetsArray;
    }

    // /**
    //  * @return Tweet[] Returns an array of Tweet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findOneBySomeField($value): ?Tweet
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.twitter_id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
