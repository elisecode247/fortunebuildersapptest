import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Tweet } from '../models/Tweet';

const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*'
  })
}
@Injectable({
  providedIn: 'root'
})

export class TwitterApiService {

  twitterApiUrl:string = 'http://127.0.0.1:8000/tweets';
  constructor(private http: HttpClient) { }

  getTweets(queryString:string):Observable<Tweet[]> {
    queryString = encodeURI(queryString).replace(/#/g, '%23');
    return this.http.get<Tweet[]>(this.twitterApiUrl+'/hashtags/'+queryString);
  }

  getSavedTweets():Observable<Tweet[]> {
    return this.http.get<Tweet[]>(this.twitterApiUrl+'/saved');
  }

  postTweet(tweet:Tweet):Observable<Tweet>{
    const url = this.twitterApiUrl+'/post';
    return this.http.post<any>(url,tweet,httpOptions);
  }

  deleteTweet(tweet:Tweet):Observable<Tweet>{
    const url = this.twitterApiUrl + '/delete/' + tweet.twitter_id;
    return this.http.delete<any>(url,httpOptions);
  }
}
