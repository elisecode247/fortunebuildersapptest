import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { TweetListComponent } from './components/tweet-list/tweet-list.component';
import { TweetComponent } from './components/tweet/tweet.component';
import { SavedTweetListComponent } from './components/saved-tweet-list/saved-tweet-list.component';
import { FormsModule } from '@angular/forms';
import { SearchTwitterComponent } from './components/search-twitter/search-twitter.component';

@NgModule({
  declarations: [
    AppComponent,
    TweetListComponent,
    TweetComponent,
    SavedTweetListComponent,
    SearchTwitterComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
