export class Tweet {
    twitter_id:string;
    name:string;
    screen_name:string;
    date_created:string;
    text:string;
}