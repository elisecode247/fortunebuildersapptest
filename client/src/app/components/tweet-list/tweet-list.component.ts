import { Component, OnInit } from '@angular/core';
import { TwitterApiService } from '../../services/twitter-api.service';
import {Tweet} from '../../models/Tweet';

@Component({
  selector: 'app-tweet-list',
  templateUrl: './tweet-list.component.html',
  styleUrls: ['./tweet-list.component.css']
})
export class TweetListComponent implements OnInit {

  tweets:Tweet[];

  constructor(private twitterApiService:TwitterApiService) {
  }

  ngOnInit() {
  }

  getTweets(hashtags:string){
    this.twitterApiService.getTweets(hashtags).subscribe(tweets => {
      this.tweets = tweets;
    });
  }

  saveTweet(tweet:Tweet) {
    this.twitterApiService.postTweet(tweet).subscribe(result => {
      alert('tweet saved!');
    });
  }

}
