import { Component, OnInit } from '@angular/core';
import { TwitterApiService } from '../../services/twitter-api.service';
import {Tweet} from '../../models/Tweet';

@Component({
  selector: 'app-saved-tweet-list',
  templateUrl: './saved-tweet-list.component.html',
  styleUrls: ['./saved-tweet-list.component.css']
})
export class SavedTweetListComponent implements OnInit {

  savedTweets:Tweet[];

  constructor(private twitterApiService:TwitterApiService) { }

  ngOnInit() {
    this.twitterApiService.getSavedTweets().subscribe(tweets => {
      this.savedTweets = tweets;
    });
  }

  deleteTweet(tweet:Tweet) {
    this.twitterApiService.deleteTweet(tweet).subscribe(result => {
      console.log('tweet deleted!');
      this.savedTweets = this.savedTweets.filter(t => t.twitter_id !== tweet.twitter_id)
    });
  }

}
