import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search-twitter',
  templateUrl: './search-twitter.component.html',
  styleUrls: ['./search-twitter.component.css']
})
export class SearchTwitterComponent implements OnInit {
  @Output() getTweets: EventEmitter<any> = new EventEmitter();

  hashtags:string;

  constructor() { }

  ngOnInit() {
  }

  searchTwitter() {
    this.getTweets.emit(this.hashtags);
  }

}
