import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Tweet } from 'src/app/models/Tweet';
import { TwitterApiService} from 'src/app/services/twitter-api.service';
@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.css']
})
export class TweetComponent implements OnInit {
  @Input() tweet:Tweet;
  @Input() isSaved:boolean;
  @Output() saveTweet:EventEmitter<Tweet> = new EventEmitter();
  @Output() deleteTweet:EventEmitter<Tweet> = new EventEmitter();
  
  constructor(private twitterApiService:TwitterApiService ) { }

  ngOnInit() {
  }

  // Set Dynamic Classes
  setClasses(){
    let classes = {
      tweet : true,
      // class names : data
    }
    return classes;
  }

  goToTwitter(tweet){
    window.open(`https://twitter.com/${tweet.name}/status/${tweet.twitter_id}`, "_blank");
  }

  save(tweet,event){
    console.log('saving');
    this.saveTweet.emit(tweet);
    event.stopPropagation();
  }

  delete(tweet,event){
    console.log('deleting');
    this.deleteTweet.emit(tweet);
    event.stopPropagation();
  }

}
