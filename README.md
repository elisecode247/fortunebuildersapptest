# Fortune Builders App Test

## Author
Elise Straub

## Project Details
  * two tabs, one for Twitter Search, second for Saved Entries
  * Include search input and results
  * Add +/- buttons for saved entries to update the database
  * Tweets can be clicked to visit it live on Twitter

## Libraries Used
  * Front-end: Angular, Bootstrap
  * Back-end: Symphony, MySql

## Tools Used
  * Visual Studio Code
  * Postman

## Resources Used
  * [Angular Crash Course](https://www.youtube.com/watch?v=Fdf5aTYRW0E&t=1653s)
  * [Symphony Tutorial](https://developer.okta.com/blog/2018/06/14/php-crud-app-symfony-vue)
  * [Decoupling Front and Back End](https://engineering.hexacta.com/decoupling-front-end-and-back-end-530d55b998a5)